// iEvents
/* LOGIN */
$(document).ready(function(){
	$(document).on('click','#frmLogin',function(){
		var usuario = $("input[name='txtUsuario']").val();
		var password = $("input[name='txtPassword']").val();
		$.ajax({
		type:'POST',
		url:'modelo/iwant/ievents/login/validarlogin.php',
		data:{usuario: usuario,password: password},
		success:function(html){
		$('#resultFrmLogin').html(html);
		}
	});
	});
});


/* TAREAS */
$(document).ready(function(){
	$(document).on('click','#frmNuevaTarea',function(){
		var fecha = $("input[name='txtFecha']").val();
		var tarea = $("input[name='txtTarea']").val();
		$.ajax({
		type:'POST',
		url:'modelo/iwant/ievents/inicio/nuevaTarea.php',
		data:{fecha: fecha,tarea: tarea},
		success:function(html){
		$('#resultFrmNuevaTarea').html(html);
		}
	});
	});
});

/* REPORTE TAREAS */
$(document).ready(function(){
	$(document).on('click','#frmReporteTarea',function(){
		var fecha = $("input[name='txtFecha']").val();
		$.ajax({
		type:'POST',
		url:'modelo/iwant/ievents/inicio/reporte_Tareas.php',
		data:{fecha: fecha},
		success:function(html){
		$('#resultFrmReporteTarea').html(html);
		}
	});
	});
});


/* ACTUALIZAR TAREAS*/
$(document).ready(function(){  
	$(document).on('click', '.edit_banner', function(){  

		var tareaId = $(this).attr("id");  
		$.ajax({  
			url:"modelo/iwant/ievents/reporte/actualizar_tareas.php",  
			method:"POST",  
			data:{tareaId:tareaId},  
			dataType:"json",  
			
			success:function(data){  
							$('#tareaid').val(data.tarId);
							$('.txtFecha').val(data.tarFecha); 
							$('#txtTarea').val(data.tarNombre); 
							$('#cmbEstado').val(data.tarEstado);
							$('#cmbRealizado').val(data.tarRealizado); 
							$('#add_data_Modal').modal('show');
				}  
			
		});
		
	});  
	
}); 

/* VALOR ACTUALIZAR TAREAS */
$(document).ready(function(){
	$(document).on('click','#frmActualizarTareas',function(){
		
		var fecha= $("input[name='txtFecha']").val();
		var tarea= $("input[name='txtTarea']").val();
		var realizo = $("select[name='cmbRealizado']").val();
		var estado = $("select[name='cmbEstado']").val();
		var valorId = $("input[name='hidId']").val();
		var valorConf = $("input[name='txtHidden']").val();
		$.ajax({
		type:'POST',
		url:'modelo/iwant/ievents/reporte/actualizar_tareas.php',
		data:{fecha: fecha,realizo: realizo,estado: estado,valorId: valorId,valorConf: valorConf,tarea: tarea},
		success:function(html){
		$('#resultFrmActualizarTareas').html(html);
		}
	});
	});
});
