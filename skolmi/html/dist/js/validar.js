function enviar(idioma,controlador){
	// Creamos el formulario auxiliar
	var idiomas=idioma;
	var controladores=controlador;
	// Creamos el formulario auxiliar
	var form = document.createElement( "form" );
	
	// Le añadimos atributos como el name, action y el method
	form.setAttribute( "name", "formulario" );
	form.setAttribute( "action", "./" );
	form.setAttribute( "method", "POST" );
	// Creamos un input para enviar el valor
						var idioma = document.createElement( "input" );
						var controlador = document.createElement( "input" );
						var session = document.createElement( "input" );
						
						// Le añadimos atributos como el name, type y el value
						session.setAttribute( "name", "txtSession" );
						session.setAttribute( "type", "hidden" );
						session.setAttribute( "value", "session" );
						idioma.setAttribute( "name", "idioma" );
						idioma.setAttribute( "type", "hidden" );
						idioma.setAttribute( "value", idiomas );
						controlador.setAttribute( "name", "iwantevents");
						controlador.setAttribute( "type", "hidden" );
						controlador.setAttribute( "value", controladores );
						
						
						// Añadimos el input al formulario
						form.appendChild( idioma );
						form.appendChild( controlador );
						form.appendChild( session );
	
	
	// Añadimos el formulario al documento
	document.getElementsByTagName( "body" )[0].appendChild( form );
	
	// Hacemos submit
	document.formulario.submit();
}


function login(f){
	var retorno = true
	var styleError = "border:1px solid #dd4b39;";
	$("input[name='txtUsuario'], input[name='txtPassword']").removeAttr("style",styleError);
	if($("input[name='txtUsuario']").val()==0){
		$("input[name='txtUsuario']").attr("style",styleError);
		retorno = false;
	}
	if($("input[name='txtPassword']").val()==0){
		$("input[name='txtPassword']").attr("style",styleError);
		retorno = false;
	}
	

	return retorno;	

}

function nuevaTarea(f){
				
	var retorno = true 
	var styleError = "border:1.5px solid #dd4b39;";
	$("input[name='txtFecha'],input[name='txtTarea']").removeAttr("style",styleError);
	
	if($("input[name='txtFecha']").val()==0){
		$("input[name='txtFecha']").attr("style",styleError);
		retorno = false;
	}
	if($("input[name='txtTarea']").val()==0){
		$("input[name='txtTarea']").attr("style",styleError);
		retorno = false;
	}
	if(retorno){
		$("#lblTextoError").hide();
	}else{
		$("#lblTextoError").show();
	}
	
	return retorno;	
}	


function nuevoIdiomaFrm(f){
				
	var retorno = true 
	var styleError = "border:1.5px solid #dd4b39;";
	$("select[name='cmbIdiomas']").removeAttr("style",styleError);
	
	if($("select[name='cmbIdiomas']").val()==0){
		$("select[name='cmbIdiomas']").attr("style",styleError);
		retorno = false;
	}
	if(retorno){
		$("#lblTextoError").hide();
	}else{
		$("#lblTextoError").show();
	}
	
	return retorno;	
}	

function actualizarTareas(f){
				
	var retorno = true 
	var styleError = "border:1.5px solid #dd4b39;";
	$("input[name='txtFecha'],input[name='txtTarea'],select[name='cmbRealizado'],select[name='cmbEstado']").removeAttr("style",styleError);
	
	if($("input[name='txtFecha']").val()==0){
		$("input[name='txtFecha']").attr("style",styleError);
		retorno = false;
	}
	if($("input[name='txtTarea']").val()==0){
		$("input[name='txtTarea']").attr("style",styleError);
		retorno = false;
	}
	if($("select[name='cmbEstado']").val()==0){
		$("select[name='cmbEstado']").attr("style",styleError);
		retorno = false;
	}
	if($("select[name='cmbRealizado']").val()==0){
		$("select[name='cmbRealizado']").attr("style",styleError);
		retorno = false;
	}
	if(retorno){
		$("#lblTextoError").hide();
	}else{
		$("#lblTextoError").show();
	}
	
	return retorno;	
}	

