 <!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      
    </ul>
  </nav>
  <!-- /.navbar -->

 
 
 <!-- Main Sidebar Container -->
 <aside class="main-sidebar elevation-4 sidebar-light-danger">
    <!-- Brand Logo -->
    <a onclick="enviar('<?php echo $idioma?>','inicio')" href="#" class="brand-link bg-gray-light">
      <img src="./html/img/logo_iglesia.png"
           alt=""
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo NOMBREPRODUCTO ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo $_SESSION['foto']?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['nombremenu']?> <?php echo $_SESSION['apellidomenu']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a onclick="enviar('<?php echo $idioma?>','inicio')" id="inicio"  href="#" class="nav-link active">
              <i class="nav-icon fa fa-th"></i>
              <p>
              <?php echo INICIO?>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="desv">
              <i class="nav-icon fa fa-edit"></i>
              <p>
              <?php echo REGISTRO?>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a onclick="enviar('<?php echo $idioma?>','tareasiEvents')" href="#" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p><?php echo TAREAS?></p>
                </a>
            </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="desv">
              <i class="nav-icon fa fa-table"></i>
              <p>
              <?php echo REPORTES?>
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a onclick="enviar('<?php echo $idioma?>','reporteTareasiEvents')" href="#" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p><?php echo TAREAS?></p>
                </a>
            </li>
            </ul>
          </li>
          <!--<li class="nav-item">
            <a href="../widgets.html" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>-->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>