<?php
$controlador=$_SESSION['universal'];
// Fichero de idioma espa&ntilde;ol.
define('LANGUAGE','Idioma');
define('SPANISH','Espa&ntilde;ol');
define('ENGLISH','Ingles');
define('APP_TITLE','Universal Sports AC');
define('APP_COPY','<p>+1 (704) 222-7785 - (980) 938-5811</p>
                        <p><small>© 2019-'.date('Y',time()).' Universal Sports AC | Dise&ntilde;o For <a href="http://iwantmark.com">iWantMark</a></small></p>');
define('FOOTER','© 2019-'.date('Y',time()).' Universal Sports AC ');	
define('ALLRIGHTS','ALL COPYRIGHTS RESERVED BY Universal Sports AC 2018 | For <a style="text-decoration:none;color:white" target="_blank"  href="http://iwantmark.com">iWantMark</a>');	
define('ADDRESS','info@universalsportsac.com');	


//MENU
define('INICIO','Home');
define('NOSOTROS','About us');
define('CONTACTENOS','Contact Us');
define('HORARIOS','Hours');
define('IMAGENES','Images');
define('COSTO','Cost');
define('EVENTOS','Events');
define('INSCRIPCION','inscription');

define('BIENVENIDOS','Welcome');
define('MSNBIENVENIDOS','Indoor synthetic field');
define('TITULONOSOTROS','<h1>Universal <br>Sports AC</h1>');
define('SUBTITULONOSOTROS','We are here to offer the best service');
define('CONTENIDONOSOTROS',' We are a sports center that has two synthetic indoor soccer courts, one for adults, one for children.<br>
It has a large space for small celebrations such as birthdays, baby showers and more. ');
define('NEGOCIO','Business');
define('TITULONEGOCIO','What we do as a business');
define('SUBTITULONEGOCIO','Our facilities have air conditioning');
define('SUBTITULO2NEGOCIO','<h1>If you have a <br>soccer academy</h1>');
define('SUBTITULO3NEGOCIO','You can think of us');
define('CONTENIDONEGOCIO','We rent our courts for your academy and are also available to the general public.<br> For the academies the price varies.');
define('NUESTROSHORARIOS','Our schedules');
define('VALORHORARIOS','  Monday - Friday          6:00 PM - 10:00 PM <br>Saturday - Sunday    10:00 AM - 10:00 PM<br> Big field      $ 90.00<br>
Small field    $ 60.00<br>If you have an academy and would like to rent the field, contact us. Prices vary.');
define('MSNCONTACTENOS','Leave us your information and we will contact you.');
define('INFORMACIONDECONTACTO','Contact information');
define('FACEBOOK','FaceBook');
define('NOMBRES','Names');
define('APELLIDOS','Last name');
define('EMAIL','Email');
define('TELEFONO','Telephone');
define('MENSAJE','Message');
define('ENVIAR','Send');
define('GALERIADEIMAGENES','Image gallery');
define('RESPUESTAEMAIL','Thanks for writing, this is the information we receive');
define('ALERTEMAIL','Thanks for writing, reviewing inbox or spam!!!');	
define('GRACIASPORESCRIBIRNOS','Thanks for writing');	


?>