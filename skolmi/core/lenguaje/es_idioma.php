<?php
$controlador=$_SESSION['legendario'];
// Fichero de idioma espa&ntilde;ol.
define('IDIOMA','Idioma');
define('ESPANOL','Espa&ntilde;ol');
define('INGLES','Ingles');
define('APP_TITLE','SKOLMI | iWantMark ');
define('APP_COPY','<div class="float-right d-none d-sm-block"><b>Versión </b>1.00001</div><strong>© 2019-'.date('Y',time()).' iEvents  | Diseñado por <a href="http://iwantmark.com">iWantMark</a></small></p>');
define('FOOTER','© 2020-'.date('Y',time()).' <a href=http://iwantmark.com target=blank_>iWantMark</a>');	
define('ALLRIGHTS','ALL COPYRIGHTS RESERVED BY iEvents | Por <a style="text-decoration:none;color:white" target="_blank" href="http://iwantmark.com">iWantMark</a>');	
define('ADDRESS','info@iwantmark.com');	
define('NOMBREPRODUCTO','skolmi');
define('BIENVENIDO','Bienvenido');

//MENU
define('INICIO','Inicio');
define('IDIOMA','Idioma');




define('NUEVOIDIOMA','Nuevo idioma');
define('IDIOMA','Idioma');
define('CODIGO','C&oacute;digo');
define('ACTUALIZARIDIOMA','Actualizar idioma');
define('CONSULTAR','Consultar');
define('REPORTEDEIDIOMAS','Reporte de idiomas');
define('LENGUAJE','Lenguaje');
define('ACTUALIZARESTADODELLENGUAJE','Actualizar estado del lenguaje');
define('BANNER','Banner');
define('INFORMACIONDELEVENTO','Información del evento');
define('NOMBREDELEVENTO','Nombre del evento');
define('LETRAS140','140 Letras');
define('IMAGEN','Imagen');
define('TAMANO1200X1000','Tamaño 1200x1000 px');
define('ERRORDEUSUARIOOCONTRASENA','Error de Usuario o Contraseña.');
define('INFODETAMANOIMAGEN','Las dimensiones de la imagen son demasiado grandes o pequeñas, El tamaño máximo es 1200x1000 px ');
define('ERRORDETIPODEARCHIVO','Error del tipo de archivo (JPG, PNG).');
define('SELECCIONARARCHIVOS','Seleccionar archivos.');
define('YAESTACREADALAENTRADAPARAESTEIDIOMAPORFAVORACTUALIZAR','Ya está creada la entrada para este lenguaje, por favor actualizarla.');
define('YAESTACREADALAENTRADAPORFAVORACTUALIZAR','Ya está creada la entrada, por favor actualizarla.');
define('INSCRIPCION','Inscripción.');
define('INFOEVENTOS','Conoce e inscribete a nuestros eventos');
define('ACTUALIZARENTRADASDELEVENTO','Actualizar entradas del evento');
define('REPORTEDEENTRADASDELBANNER','Reporte de entradas del banner');










define('NOSOTROS','Nosotros');
define('CONTACTENOS','Contáctenos');
define('HORARIOS','Horarios');
define('IMAGENES','Imágenes');
define('COSTO','Costo');
define('EVENTOS','Eventos');
define('ADMINISTRAR','Administrar');
define('REGISTRO','Registro');
define('NUMEROREC','Código REC');
define('NUMEROTRAININGDAY','Código Training day');
define('IGLESIA','Iglesia');
define('ENFERMEDAD','Enfermedad');
define('TALLADECAMISA','Talla de Camisa');
define('REPORTES','Reportes');
define('GENERAL','General');
define('TIPODEPAGO','Tipo de Pago');
define('AREA','Área');
define('ESTADOCIVIL','Estado Civil');

define('DATOSPERSONALES','Datos Personales');
define('CEDULA','Cédula');
define('FOTOCEDULA','Foto Cédula');
define('FECHANACIMIENTO','Fecha Nacimiento');
define('PROFESION','Profesión / Ocupación');
define('PAIS','País');
define('TALLADECAMISETA','Talla de Camiseta');
define('ENFERMEDADES','Enfermedades');
define('DETALLEENFERMEDADES','Detalles Enfermedades');
define('IGLESIAQUEPERTENECE','Iglesia que Pertenece');
define('DATOSDEPAGO','Datos del Pago');
define('NUMERODETRANSACCION','Número de transacción');
define('FOTOCOMPROBANTE','Foto Comprobante');
define('DATOSDECONTACTO','Datos del Contacto');
define('NOMBRESYAPELLIDOS','Nombre y Apellidos');

define('BIENVENIDOS','Bienvenido');
define('MSNBIENVENIDOS','Canchas sintéticas bajo techo');
define('TITULONOSOTROS','<h1>Universal <br>Sports AC</h1>');
define('SUBTITULONOSOTROS','Estamos aquí para ofrecer el mejor servicio');
define('CONTENIDONOSOTROS',' Somos un centro deportivo que cuenta con dos canchas sintéticas para futbol bajo techo, una para adultos, otra para niños.<br>
Cuenta con un espacio amplio para pequenas celebraciones como, cumpleaños, baby showers y más.');
define('NEGOCIO','Negocio');
define('TITULONEGOCIO','Lo que hacemos como negocio');
define('SUBTITULONEGOCIO','Nuestras instalaciones cuentan con aire acondicionado');
define('SUBTITULO2NEGOCIO','<h1>Si tiene una académia <br>de Futbol</h1>');
define('SUBTITULO3NEGOCIO','Puede pensar en nosotros');
define('CONTENIDONEGOCIO','Le rentamos nuestras canchas para su académia y tambien están a disposición del publico en general.<br>Para las academias el precio varía.');
define('NUESTROSHORARIOS','Nuestros horarios');
define('VALORHORARIOS','  Lunes - Viernes           6:00 PM - 10:00 PM <br>Sabado - Domingo    10:00 AM - 10:00 PM <br> Cancha Grande      $ 90.00<br>
Cancha Pequeña    $ 60.00<br>Si tienes tu académia y te gustaria rentarla comunicate con nosotros , precios varian.');
define('MSNCONTACTENOS','Déjanos tus datos y nos comunicaremos contigo.');
define('INFORMACIONDECONTACTO','Información de contacto');
define('FACEBOOK','FaceBook');
define('NOMBRES','Nombres');
define('APELLIDOS','Apellidos');
define('EMAIL','Email');
define('TELEFONO','Teléfono');
define('MENSAJE','Mensaje');
define('ENVIAR','Enviar');
define('GALERIADEIMAGENES','Galeria de imágenes');
define('RESPUESTAEMAIL','Gracias por escribirnos, esta es la informaci&oacute;n que recibimos');
define('ALERTEMAIL','Gracias por escribirnos, revisar bandeja de entrada o spam!!!');
define('GRACIASPORESCRIBIRNOS','Gracias por escribirnos');	
define('SELECCIONAR','Seleccionar');
define('INSCRIPCIONESREC','INCRIPCIONES REC');
define('NUMERODELEGENDARIO','Número de legendario ');
define('INSCRIPCIONESTRAININGDAY','INCRIPCIONES TRAINING DAY');

//LOGIN
define('REGISTRARSE','Registrarse');
define('USUARIOSREGISTRADOS','Usuarios registrados');
define('REGISTROLEGENDARIOS','Registro legendarios');
define('BIENVENIDOSALEGENDARIOSLAFRATERNIDAD','Bienvenidos a legendarios - La Fraternidad');
define('CIUDAD','Ciudad');
define('SECTOR','Sector');
define('NOMBRECOMPLETO','Nombre completo');
define('APELLIDO','Apellido');
define('REGISTROREC','Registro Reto Extremo de Carácter (REC)');
define('REGISTRORECA','Registro (REC)');
define('REGISTRTRAININGDAY','Registro Training Day');
define('CEDULAONOMBRE','Cédula o Nombre');
define('REGISTROSERVIDORESREC','Registro Servidores (REC)');
define('TIENENUMERODELEGENDARIO','Tiene Número de legendario');
define('SI','Si');
define('NO','No');
define('USUARIO','Usuario');
define('CONTRASENA','Contraseña');
define('FECHAINICIAL','Fecha inicial');
define('FECHAFINAL','Fecha final');
define('NUEVOCODIGOREC','Nuevo código REC');
define('NUEVOCODIGOTRAININGDAY','Nuevo código Training day');
define('NUEVOTIPODEPAGO','Nuevo Tipo de Pago');
define('NUEVAIGLESIA','Nueva Iglesia');
define('NUEVAENFERMEDAD','Nueva Enfermedad');
define('NUEVAAREA','Nueva Área');
define('NUEVOESTADOCIVIL','Nuevo Estado Civil');
define('YAESTAENLABASEDEDATOS','Ya se encuentra registrado en la base de datos.');
define('AGREGADOCONEXITO','Agregado con Éxito.');
define('ERRORDECONEXION','Error de Conexión.');
define('NUEVATALLADECAMISA','Nueva Talla de Camisa');
define('REPORTEGENERAL','Reporte General');
define('TIPODEREPORTE','Tipo de Reporte');
define('REPORTENUMEROREC','Reporte número REC');
define('IDENTIFICADOR','Identificador');
define('ESTADO','Estado');
define('ACTUALIZARINFORMACIONDENUMEROREC','Actualizar información número REC');
define('ACTUALIZARINFORMACIONDENUMEROTRAININGDAY','Actualizar información número Training day');
define('ACTUALIZAR','Actualizar');
define('CERRAR','Cerrar');
define('SELECCIONARESTADO','Seleccionar estado');
define('ACTIVO','Activo');
define('INACTIVO','Inactivo');
define('IDIOMA','Idioma');
define('ACTUALIZARINFORMACIONTIPODEPAGO','Actualizar información Tipo de pago');
define('REPORTETIPODEPAGO','Reporte Tipo de pago');
define('REPORTEIGLESIAS','Reporte Iglesias');
define('IGLESIAS','Iglesias');
define('ACTUALIZARINFORMACIONDEENFERMEDAD','Actualizar información de Enfermedad');
define('REPORTEENFERMEDADES','Reporte Enfermedades');
define('ACTUALIZARINFORMACIONDEAREAS','Actualizar información de Áreas');
define('REPORTEDEAREAS','Reporte de Áreas');
define('ACTUALIZARINFORMACIONDEESTADOCIVIL','Actualizar información de Estado Civil');
define('REPORTEESTADOCIVIL','Reporte de Estado Civil');
define('ACTUALIZARINFORMACIONDETALLASDECAMISA','Actualizar información de Tallas de Camisas');
define('REPORTETALLASDECAMISAS','Reporte de Tallas de Camisas');
define('REPORTEREGISTRODELEGENDARIOS','Reporte Registro de Legendarios');
define('REPORTEREGISTROTRAININGDAY','Reporte Registro Training day');
define('NLEGENDARIO','#Legendario');
define('TALLA','Talla');
define('NOMBRE','Nombre');
define('FECHA','Fecha');
define('URLCEDULA','Url Cédula');
define('URLCOMPROBANTE','Url Comprobante');
define('NREGISTRO','#Registro');
define('NOMBREDECONTACTO','Nombre contacto');
define('OBSERVACION','Observación');
define('LINK','Link');
define('REPORTEDELEGENDARIOSREGISTRADOS','Reporte de Legendarios registrados');
define('ACTUALIZARINFORMACIONDELEGENDARIO','Actualizar información de Legendario');
define('OPCION','Opción');
define('ACTUALIZARINFORMACION','Actualizar información');
define('NOHASIDOPOSIBLESUBIRELFICHERO','No ha sido posible subir el fichero');
define('NOEXISTEDIRECTORIOESPECIFICADO','No existe el directorio especificado');
define('ELUSUARIOOCONTRASENASONINCORRECTOS','El usuario o contraseña son incorrectos(FTP)');
define('NOHASIDOPOSIBLECONECTARCONELSERVIDOR','No ha sido posible conectar con el servidor');
define('SELECCIONEUNARCHIVO','Selecciona un archivo...');
define('REPORTEREGISTROREC','Reporte registro Reto Extremo de Carácter (REC)');
define('REPORTEREGISTROSERVIDORESREC','Reporte registro servidores (REC)');



define('ADMINISTRACION','Administración');
define('BIENVENIDOSIEVENTS','Bienvenidos a iEvents | iWantMark');

define('TAREAS','Tareas');
define('TAREA','Tarea');

define('NUEVATAREA','Nueva tarea');
define('REPORTEDETAREAS','Reporte de tareas');
define('ACTUALIZARINFORMACIONDETAREAS','Actualizar información de tarea');
define('INFORMACIONDELATAREAS','Información de la tarea');
define('SI','Si');
define('NOS','No');
define('REALIZADO','Realizado');







?>